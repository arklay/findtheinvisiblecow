﻿#if DEBUG
//#define LOG_CONSOLE_Controllers
//#define LOG_GUI_Controllers
#endif
using System;
using UnityEngine;

namespace U3D.SteamVR
{
    public class Controllers : MonoBehaviourSingleton<Controllers>
    {
        #region log stuff
        [System.Diagnostics.Conditional("LOG_CONSOLE_Controllers")]
        void Log(string format, params object[] args)
        {
            Debug.LogFormat(this.GetType().Name + "\t" + format, args);
        }

        [System.Diagnostics.Conditional("LOG_GUI_Controllers")]
        void GUILog(string format, params object[] args)
        {
#if LOG_GUI_Controllers
				m_logStr = string.Format(format, args);
#endif
        }
#if LOG_GUI_Controllers
			string m_logStr = "";

			public void OnGUI()
			{
				GUIStyle style = new GUIStyle();
				style.normal.textColor = Color.red;
				style.alignment = TextAnchor.LowerLeft;
				GUI.Label(new Rect(5, 5, Screen.width - 10, Screen.height - 10), m_logStr, style);
			}
#endif
        #endregion

        SteamVR_ControllerManager _controllerManager;
        SteamVR_ControllerManager m_controllerManager
        {
            get { return _controllerManager ?? (_controllerManager = FindObjectOfType<SteamVR_ControllerManager>()); }
        }
        SteamVR_TrackedObject[] __trackedObjects;
        public SteamVR_TrackedObject[] trackedObjects
        {
            get
            {
                if (__trackedObjects == null)
                {
                    __trackedObjects = new SteamVR_TrackedObject[2];
                    __trackedObjects[0] = null;
                    __trackedObjects[1] = null;
                }
                if (__trackedObjects[0] == null && m_controllerManager.left != null)
                    __trackedObjects[0] = m_controllerManager.left.GetComponent<SteamVR_TrackedObject>();
                if (__trackedObjects[1] == null && m_controllerManager.right != null)
                    __trackedObjects[1] = m_controllerManager.right.GetComponent<SteamVR_TrackedObject>();
                return __trackedObjects;
            }
        }

        SteamVR_Controller.Device __leftController;
        SteamVR_Controller.Device m_leftController { get { return __leftController ?? (__leftController = trackedObjects[0].index == SteamVR_TrackedObject.EIndex.None ? null : SteamVR_Controller.Input((int)trackedObjects[0].index)); } }
        SteamVR_Controller.Device __rightController;
        SteamVR_Controller.Device m_rightController { get { return __rightController ?? (__rightController = trackedObjects[1].index == SteamVR_TrackedObject.EIndex.None ? null : SteamVR_Controller.Input((int)trackedObjects[1].index)); } }

        public event Action anyTrigger;

        public void Update()
        {
            if (anyTrigger != null)
            {
                if ((m_leftController != null && m_leftController.GetPressDown(Valve.VR.EVRButtonId.k_EButton_SteamVR_Trigger)) || 
                    (m_rightController != null && m_rightController.GetPressDown(Valve.VR.EVRButtonId.k_EButton_SteamVR_Trigger)))
                {
                    anyTrigger();
                }
            }
        }
    }
}