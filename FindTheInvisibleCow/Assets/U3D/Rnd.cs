﻿using UnityEngine;

namespace U3D
{
    public static class Rnd
    {
        static bool mInitialized = false;
        static void Initialize()
        {
            if (mInitialized) return; mInitialized = true;
            UnityEngine.Random.seed = System.DateTime.Now.Millisecond;
        }

        public static int Range(int inclusiveLower, int exclusiveUpper)
        {
            return UnityEngine.Random.Range(inclusiveLower, exclusiveUpper);
        }
        public static float Range(float inclusiveLower, float exclusiveUpper)
        {
            return UnityEngine.Random.Range(inclusiveLower, exclusiveUpper);
        }

        public static Vector3 Range(Vector3 min, Vector3 max)
        {
            return new Vector3(
                Range(min.x, max.x),
                Range(min.y, max.y),
                Range(min.z, max.z)
                );
        }

        public static Color Range(Color min, Color max)
        {
            return new Color(
                Range(min.r, max.r),
                Range(min.g, max.g),
                Range(min.b, max.b),
                Range(min.a, max.a)
                );
        }
    }
}