﻿using UnityEngine;
using System;
using System.Collections.Generic;
using UnityEngine.Events;

namespace U3D
{
    public abstract class GenericPool<T> : MonoBehaviourBase where T : Component
    {
        public int initialPooledObjects = 50;

        public virtual void Awake()
        {
            if (inactiveObjectsParent == null)
            {
                inactiveObjectsParent = new GameObject("_Inactive").transform;
                inactiveObjectsParent.transform.SetParent(this.transform, false);
                inactiveObjectsParent.transform.localPosition = Vector3.zero;
                inactiveObjectsParent.transform.localEulerAngles = Vector3.zero;
                inactiveObjectsParent.transform.localScale = Vector3.one;
            }
            if (activeObjectsParent == null)
            {
                activeObjectsParent = new GameObject("_Active").transform;
                activeObjectsParent.transform.SetParent(this.transform, false);
                activeObjectsParent.transform.localPosition = Vector3.zero;
                activeObjectsParent.transform.localEulerAngles = Vector3.zero;
                activeObjectsParent.transform.localScale = Vector3.one;
            }

            activeObjects = new List<T>();
            inactiveObjects = new List<T>();
        }
        public virtual void Start()
        {
            for (int i = 0; i < initialPooledObjects; i++)
            {
                CreateNewObjectInPool();
            }
        }

        protected T GetFirstObjectFromPool()
        {
            return GetIndexObjectFromPool(0);
        }
        protected T GetRandomObjectFromPool()
        {
            return GetIndexObjectFromPool(Rnd.Range(0, inactiveObjects.Count));
        }
        protected virtual T GetIndexObjectFromPool(int index)
        {
            while (inactiveObjects.Count <= index)
            {
                CreateNewObjectInPool();
            }

            T ret = inactiveObjects[index].GetComponent<T>();
            ActivateObject(ret);
            return ret;
        }

        public UnityEventComponent onActivated;
        public UnityEventComponent onDeactivated;

        protected virtual void ActivateObject(T item)
        {
            item.transform.SetParent(activeObjectsParent, false);
            activeObjects.Add(item);
            inactiveObjects.Remove(item);
            item.gameObject.SetActive(true);
            onActivated.Invoke(item);
        }

        public T objectPrefab;
        public Transform inactiveObjectsParent;
        public Transform activeObjectsParent;
        public List<T> inactiveObjects { get; private set; }
        public List<T> activeObjects { get; private set; }
        int m_objectCount = 0;
        protected virtual T CreateNewObjectInPool()
        {
            T newObject = Instantiate<T>(objectPrefab);
            newObject.gameObject.name = objectPrefab.gameObject.name + "_" + (++m_objectCount);
            AddObjectToPool(newObject);
            return newObject;
        }

        protected void AddObjectToPool(T newObject)
        {
            newObject.transform.SetParent(inactiveObjectsParent, false);
            inactiveObjects.Add(newObject);
            newObject.transform.gameObject.SetActive(false);
        }

        protected virtual void ReturnToAllPool()
        {
            T[] active = activeObjects.ToArray();
            foreach (T i in active)
            {
                DeactivateObject(i);
            }
        }
        protected virtual void ReturnToPool(T item)
        {
            DeactivateObject(item);
        }
        protected virtual void DeactivateObject(T item)
        {
            item.transform.SetParent(inactiveObjectsParent, false);
            activeObjects.Remove(item);
            inactiveObjects.Add(item);
            item.gameObject.SetActive(false);

            onDeactivated.Invoke(item);
        }
    }
}