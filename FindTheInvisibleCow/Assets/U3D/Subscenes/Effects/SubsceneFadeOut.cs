﻿using System;
using System.Collections.Generic;
using UnityEngine;
using U3D.Threading.Tasks;

namespace U3D.Subscenes.Effects
{
    public class SubsceneFadeOut : MonoBehaviourBase, ISubsceneHide
    {
        public List<CanvasGroup> targets = new List<CanvasGroup>();
        public float seconds = 1;
        float m_currentTime;
        TaskCompletionSource<bool> m_tcs;
        public Task Hide(bool invert)
        {
            m_currentTime = 0;
            m_tcs = new TaskCompletionSource<bool>();
            this.enabled = true;
            targets.ForEach(c => c.alpha = 1);

            return m_tcs.Task;
        }

        public void Update()
        {
            if (m_tcs == null)
            {
                this.enabled = false;
                return;
            }

            m_currentTime += Time.deltaTime;
            targets.ForEach(c =>
            {
                c.alpha = Mathf.Lerp(1, 0, m_currentTime / seconds);
            });
            if (m_currentTime >= seconds)
            {
                m_tcs.SetResult(true);
                m_tcs = null;
            }
        }
    }
}