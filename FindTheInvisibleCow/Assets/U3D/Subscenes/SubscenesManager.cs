﻿#if DEBUG
//#define DEBUG_SubscenesManager
#endif

using UnityEngine;
using System.Collections;
using U3D.Threading.Tasks;
using System;
using UnityEngine.Analytics;
using System.Collections.Generic;
using System.Text;
using UnityEngine.UI;

namespace U3D.Subscenes
{
    public interface ISubsceneShow
    {
        Task Show(bool invert);
    }
    public interface ISubsceneHide
    {
        Task Hide(bool invert);
    }

    public class SubscenesManager : MonoBehaviourBase
    {
        [System.Diagnostics.Conditional("DEBUG_SubscenesManager")]
        static void Log(string format, params object[] args)
        {
            Debug.LogFormat(format, args);
        }

        public Transform initialSubscene = null;
        Stack<Transform> m_prevScenes = new Stack<Transform>();
        Transform __currentSubscene = null;
        Transform m_currentSubscene
        {
            get { return __currentSubscene; }
            set
            {
                if (__currentSubscene != null)
                {
                    m_prevScenes.Push(__currentSubscene);
                }
                __currentSubscene = value;
            }
        }

        public List<GameObject> deactivateWhileTransitioning;

        public void Start()
        {
            foreach (Transform t in this.transform)
            {
#if DEBUG
                initialSubscene = t; // if debug use whatever we are testing (whatever is ON)
#endif
                t.gameObject.SetActive(false);
            }

            GoToSubscene(initialSubscene);
        }

        public void GoToSubscene(Transform nextSubscene)
        {
            GoToSubscene(nextSubscene, null);
        }
        public void GoToSubscene(Transform nextSubscene, Action whenDone, bool invert = false, Action whenWillShow = null)
        {
            Log("Going to next: {0} ({1})", nextSubscene.gameObject.name, this);
            deactivateWhileTransitioning.ForEach(g => g.SetActive(false));
            DoGoToSubscene(nextSubscene, () =>
            {
                Log("Went to: {0} ({1})", nextSubscene.gameObject.name, this);
                deactivateWhileTransitioning.ForEach(g => g.SetActive(true));
                if (whenDone != null) whenDone();
            }, invert, whenWillShow);
        }

        class DefaultHide : ISubsceneHide
        {
            public Task Hide(bool invert)
            {
                return Task.CompletedTask;
            }
        }
        class DefaultShow : ISubsceneShow
        {
            public Task Show(bool invert)
            {
                return Task.CompletedTask;
            }
        }

        void DoGoToSubscene(Transform nextSubscene, Action whenDone, bool invert, Action whenWillShow)
        {
            Log("DoGoToSubscene {0} ({1})", nextSubscene, m_currentSubscene);
            if(m_currentSubscene != null)
            {
                ISubsceneHide hide = GetInterfaceComponent<ISubsceneHide>(m_currentSubscene);
                if (hide == null)
                {
                    hide = new DefaultHide();
                }
                hide.Hide(invert).ContinueInMainThreadWith((t) =>
                {
                    m_currentSubscene.gameObject.SetActive(false);
                    m_currentSubscene = null;
                    if (nextSubscene == null)
                    {
                        whenDone();
                    }
                    else
                    {
                        DoGoToSubscene(nextSubscene, whenDone, invert, whenWillShow);
                    }
                });
            }
            else
            {
                ISubsceneShow show = GetInterfaceComponent<ISubsceneShow>(nextSubscene);
                if (show == null)
                {
                    show = new DefaultShow();
                }

                if (whenWillShow != null) whenWillShow();
                nextSubscene.gameObject.SetActive(true);
                show.Show(invert).ContinueInMainThreadWith((t2) =>
                {
                    m_currentSubscene = nextSubscene;
                    whenDone();
                });
            }
        }

        public void GoToPreviousSubScene()
        {
            GoToPreviousSubScene(null);
        }
        public void GoToPreviousSubScene(Action whenDone)
        {
            Log("Going to previous: ({0})", this);
            Transform targetScene = initialSubscene;
            if (m_prevScenes.Count>0)
                targetScene = m_prevScenes.Pop();
            GoToSubscene(targetScene, () =>
            {
                Log("Went to previous: ({0})", this);
                if(m_prevScenes.Count > 0) m_prevScenes.Pop(); // remove the added current scene

                if(whenDone!=null) whenDone();
            }, true);
        }

        public void Exit()
        {
            GoToSubscene(null, () =>
            {
                Debug.Log("EXIT");
                Application.Quit();
            }, true);
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("[");
            sb.Append(m_currentSubscene == null ? "null" : m_currentSubscene.gameObject.name);
            sb.Append(",<");
            foreach(Transform i in m_prevScenes)
            {
                sb.Append(i.gameObject.name);
                sb.Append(",");
            }
            sb.Append(">]");
            return sb.ToString();
        }
    }
}