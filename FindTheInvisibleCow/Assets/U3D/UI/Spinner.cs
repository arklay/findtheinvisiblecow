﻿
namespace U3D.UI
{
    public abstract class Spinner : MonoBehaviourBase
    {
        protected bool _playing
        {
            get; private set;
        }

        public void Play()
        {
            _playing = true;
        }

        public void Pause()
        {
            _playing = false;
        }
    }
}