﻿using UnityEngine;
using System.Collections;
using UnityEditor;

namespace U3D.UI
{
    [CanEditMultipleObjects]
    [CustomEditor(typeof(ProgresiveSinLineSpinner), true)]
    public class ProgresiveSpinnerInspector : Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            if (GUILayout.Button("Refresh"))
            {
                ProgresiveSinLineSpinner src = target as ProgresiveSinLineSpinner;
                src.Refresh();
                src.Update();
            }
        }
    }
}