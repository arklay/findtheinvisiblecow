﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

namespace U3D.UI
{
    [RequireComponent(typeof(Button))]
    public class KeyForButton : MonoBehaviourBase
    {
        Button __button;
        Button m_button { get { return __button ?? (__button = GetComponent<Button>()); } }
        public KeyCode key;
        public void Update()
        {
            if(Input.GetKeyUp(key))
            {
                m_button.onClick.Invoke();
            }
        }
    }
}