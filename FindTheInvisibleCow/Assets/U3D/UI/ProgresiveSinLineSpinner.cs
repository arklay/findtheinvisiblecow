﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System;

namespace U3D.UI
{
    [ExecuteInEditMode]
    public class ProgresiveSinLineSpinner : Spinner
    {
        [Range(0, 30)]
        public float speed = 1;
        [Range(1, 30)]
        public int nBalls = 10;
        [Range(1, 1000)]
        public float ballSeparation = 10;
        public Color color;

        public Image ball;
        public enum TNBallsType { fixedNumber, fixedSeparation }
        public TNBallsType nBallsType = TNBallsType.fixedNumber;

        List<RectTransform> __balls;
        List<RectTransform> _balls
        {
            get
            {
                if (__balls == null)
                {
                    __balls = new List<RectTransform>();

                    List<GameObject> toRemove = new List<GameObject>();
                    for (int i = 0; i < transform.childCount; i++)
                    {
                        GameObject go = transform.GetChild(i).gameObject;
                        if (go.name.StartsWith("ball"))
                        {
                            toRemove.Add(go);
                        }
                    }
                    foreach (GameObject i in toRemove)
                        GameObject.DestroyImmediate(i);

                    float deltaX = 0;
                    if (nBalls != 1)
                    {
                        deltaX = -this._rectTransform.sizeDelta.x / 2f;
                        switch (nBallsType)
                        {
                            case TNBallsType.fixedNumber:
                                ballSeparation = this._rectTransform.rect.width / (nBalls - 1);
                                break;
                            case TNBallsType.fixedSeparation:
                                nBalls = Mathf.FloorToInt(this._rectTransform.rect.width / ballSeparation) + 1;
                                break;
                        }
                    }
                    deltaX -= _rectTransform.anchoredPosition.x;
                    for (int i = 0; i < nBalls; i++)
                    {
                        Image b = Instantiate<Image>(ball);
                        RectTransform bRectTransform = b.rectTransform;

                        b.gameObject.name = "ball";
                        __balls.Add(bRectTransform);
                        bRectTransform.SetParent(this.transform, false);
                        bRectTransform.localScale = Vector3.one;
                        b.color = this.color;
                        Vector3 pos = Vector3.zero;
                        pos.x = deltaX;
                        deltaX += ballSeparation;
                        bRectTransform.localPosition = pos;
                    }
                }
                return __balls;
            }
        }
        float timeStep = 0;

        RectTransform __rectTransform;
        RectTransform _rectTransform
        {
            get
            {
                if (!Application.isPlaying) return GetComponent<RectTransform>();

                return __rectTransform ?? (__rectTransform = GetComponent<RectTransform>());
            }
        }

        public void Refresh()
        {
            __balls = null;
        }

        public void Update()
        {
            for (int i = 0; i < _balls.Count; i++)
            {
                RectTransform t = _balls[i];
                Vector3 cPos = t.localPosition;
                cPos.y = GetNewY(i, timeStep);
                t.localPosition = cPos;
            }

            if (_playing)
                timeStep += Time.deltaTime;
        }

        float GetNewY(int ballIndex, float currentTime)
        {
            float xLocationModifier = (float)ballIndex / ((float)_balls.Count);
            float timeAndLocation = speed * currentTime * (xLocationModifier + 1);

            float sinusPart = timeAndLocation * Mathf.PI * 2;

            return Mathf.Sin(sinusPart) * this._rectTransform.sizeDelta.y / 2f;
        }
    }
}