﻿using UnityEngine;

namespace U3D.Extensions
{
    public static class Vector2Extensions
    {
        public static Vector2 Multiply(this Vector2 me, Vector2 other)
        {
            return new Vector2(me.x * other.x, me.y * other.y);
        }
        public static Vector2 Divide(this Vector2 me, Vector2 other)
        {
            return new Vector2(me.x / other.x, me.y / other.y);
        }
    }
}