﻿using UnityEngine;

namespace U3D.Extensions
{
    public static class ColorExtensions
    {
        public static Color Interpolate(Color[] colors, float value)
        {
            float intervals = 1f / (colors.Length - 1);
            int c1 = 0, c2 = 1;
            value = Mathf.Clamp01(value);
            float curr = intervals;
            while (value > curr)
            {
                curr += intervals;
                c1++;
                c2++;
            }
            curr -= intervals;
            return Color.Lerp(colors[c1], colors[c2], (value - curr) / (intervals));
        }
    }
}