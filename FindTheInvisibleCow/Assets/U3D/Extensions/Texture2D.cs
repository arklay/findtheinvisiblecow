﻿using System;
using System.Collections;
using System.Net;
using System.Net.Security;
using U3D.Threading.Tasks;
using UnityEngine;

namespace U3D.Extensions
{
    public static class TextureExtensions
    {
        static TextureExtensions()
        {
            ServicePointManager.ServerCertificateValidationCallback += new RemoteCertificateValidationCallback((sender, certificate, chain, policyErrors) => { return true; });
        }

        public static IEnumerator LoadFromURL(this Texture t, string url, Action<bool> whenDone)
        {
            WWW www = new WWW(url);
            yield return www;

            if (!string.IsNullOrEmpty(www.error))
            {
                if (whenDone != null)
                    whenDone(true);
            }
            else
            {
                www.LoadImageIntoTexture((Texture2D)t);
                if (whenDone != null)
                    whenDone(false);
            }
        }
        public static Task<Texture> GetFromURL(this Texture t, string url)
        {
            TaskCompletionSource<Texture> tcs = new TaskCompletionSource<Texture>();
            Task.Run(() =>
            {
                try
                {
                    WebClient c = new WebClient();
                    byte[] responseData = c.DownloadData(url);
                    Task.RunInMainThread(() =>
                    {
                        try
                        {
                            if (t == null)
                                t = new Texture2D(1, 1);
                            ((Texture2D)t).LoadImage(responseData);
                            tcs.SetResult(t);
                        }
                        catch (Exception ex2)
                        {
                            Debug.LogWarningFormat("Error loading {0} => {1}", url, ex2.ToString());
                            tcs.SetError(ex2);
                        }
                    });
                }
                catch (Exception ex)
                {
                    Debug.LogWarningFormat("Error downloading {0} => {1}", url, ex.ToString());
                    tcs.SetError(ex);
                }
            });
            return tcs.Task;
        }
    }
}