﻿using UnityEngine;

namespace U3D
{
    public abstract class MonoBehaviourSingleton<T> : MonoBehaviourBase where T : MonoBehaviourSingleton<T>
    {
        static T __instance = null;
        public static T instance
        {
            get
            {
                if (__instance != null) return __instance;

                __instance = FindObjectOfType<T>();
                if (__instance == null)
                {
                    __instance = new GameObject(typeof(T).Name).AddComponent<T>();
                }
                Debug.Assert(__instance != null, "Cannot create singleton for " + typeof(T).Name);

                DontDestroyOnLoad(__instance.gameObject);
                __instance.InitializeInstance();
                return __instance;
            }
            private set
            {
                if (__instance == value)
                    return;
                if (__instance != null)
                    throw new System.InvalidOperationException("Two singletons in the scene!");
                __instance = value;
                DontDestroyOnLoad(__instance.gameObject);
                __instance.InitializeInstance();
            }
        }
        public virtual void Awake()
        {
            instance = (T)this;
        }

        protected virtual void InitializeInstance()
        {

        }
    }
}