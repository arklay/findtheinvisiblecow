﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace U3D
{
    public abstract class GenericPoolIndexed<T> : GenericPool<T> where T : Component
    {
        protected Dictionary<string, T> m_activeObjects { get; private set; }
        protected Dictionary<string, T> m_inactiveObjects { get; private set; }
        public override void Awake()
        {
            base.Awake();
            m_activeObjects = new Dictionary<string, T>();
            m_inactiveObjects = new Dictionary<string, T>();
        }

        protected override void ActivateObject(T item)
        {
            base.ActivateObject(item);
            m_activeObjects.Add(item.name, item);
            m_inactiveObjects.Remove(item.name);
        }
        protected override void DeactivateObject(T item)
        {
            base.DeactivateObject(item);
            m_activeObjects.Remove(item.name);
            m_inactiveObjects.Add(item.name, item);
        }

        public virtual T FindAndActivate(string objectName, bool create = true)
        {
            T ret = null;
            if (m_activeObjects.ContainsKey(objectName))
            {
                ret = m_activeObjects[objectName];
            }
            else if (m_inactiveObjects.ContainsKey(objectName))
            {
                ret = m_inactiveObjects[objectName];
                ActivateObject(ret);
            }
            else if (create)
            {
                ret = CreateNewObjectInPool();
                ret.gameObject.name = objectName;
                ActivateObject(ret);
            }
            return ret;
        }

        protected void RefreshDictionaries()
        {
            RefreshDictionary(m_activeObjects);
            RefreshDictionary(m_inactiveObjects);
        }

        private void RefreshDictionary(Dictionary<string, T> dic)
        {
            List<T> toUpdate = new List<T>();
            List<string> toRemove = new List<string>();
            foreach (string k in dic.Keys)
            {
                if (k != dic[k].gameObject.name)
                {
                    toUpdate.Add(dic[k]);
                    toRemove.Add(k);
                }
            }
            toRemove.ForEach((s) => dic.Remove(s));
            toUpdate.ForEach((t) => dic.Add(t.gameObject.name, t));
        }
    }
}