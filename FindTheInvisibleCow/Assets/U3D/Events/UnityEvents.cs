﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace UnityEngine.Events
{
    [Serializable]
    public class UnityEventFloat : UnityEvent<float> { }
    [Serializable]
    public class UnityEventInt : UnityEvent<int> { }
    [Serializable]
    public class UnityEventVector2 : UnityEvent<Vector2> { }
    [Serializable]
    public class UnityEventBool : UnityEvent<bool> { }
    [Serializable]
    public class UnityEventString : UnityEvent<string> { }
    [Serializable]
    public class UnityEventTexture2D : UnityEvent<Texture2D> { }
    [Serializable]
    public class UnityEventComponent : UnityEvent<Component> { }

}