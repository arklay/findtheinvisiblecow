﻿using System;
using UnityEngine;

[RequireComponent(typeof(Animation))]
public class HideShowCanvas : MonoBehaviourBase
{

    Animation __animation;
    Animation m_animation { get { return __animation ?? (__animation = GetComponent<Animation>()); } }
    public void Show()
    {
        m_animation.gameObject.SetActive(true);
        m_animation.Play("Show");
    }
    public void Hide()
    {
        if(this!=null)
            m_animation.Play("Hide");
    }
    public void Hidden()
    {
        this.gameObject.SetActive(false);
    }
}
