﻿#if DEBUG
//#define LOG_CONSOLE_Cow
//#define LOG_GUI_Cow
#endif
using System;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SphereCollider))]
public class Cow : MonoBehaviourBase
{
    #region log stuff
    [System.Diagnostics.Conditional("LOG_CONSOLE_Cow")]
    void Log(string format, params object[] args)
    {
        Debug.LogFormat(this.GetType().Name + "\t" + format, args);
    }

    [System.Diagnostics.Conditional("LOG_GUI_Cow")]
    void GUILog(string format, params object[] args)
    {
#if LOG_GUI_Cow
        m_logStr = string.Format(format, args);
#endif
    }
#if LOG_GUI_Cow
    string m_logStr = "";

    public void OnGUI()
    {
        GUIStyle style = new GUIStyle();
        style.normal.textColor = Color.red;
        style.alignment = TextAnchor.LowerLeft;
        GUI.Label(new Rect(5, 5, Screen.width - 10, Screen.height - 10), m_logStr, style);
    }

#endif
    #endregion

    public void OnDrawGizmos()
    {
        Gizmos.color = Color.black;
        Gizmos.DrawSphere(m_cowPosition, 0.1f);
        Gizmos.color = Color.red;
        m_impactPoints.ForEach(p => Gizmos.DrawSphere(p, 0.1f));
    }

    AudioSource __audioSource;
    AudioSource m_audioSource { get { return __audioSource ?? (__audioSource = foundCow.GetComponent<AudioSource>()); } }

    SphereCollider __sphere;
    SphereCollider m_sphere { get { return __sphere ?? (__sphere = GetComponent<SphereCollider>()); } }

    Vector3 m_cowPosition;
    public FoundCow foundCow;
    public void OnEnable()
    {
        m_cowPosition = UnityEngine.Random.onUnitSphere * m_sphere.radius;
        m_cowPosition.y = Mathf.Abs(m_cowPosition.y);
        foundCow.enabled = false;
        foundCow.transform.position = m_cowPosition;
        foundCow.transform.LookAt(this.transform);

        m_maxDistance = m_sphere.radius * Mathf.PI / 2;
    }

    List<Vector3> m_impactPoints = new List<Vector3>();
    public List<AudioClip> clips;
    float m_maxDistance;
    public void Update()
    {
        float dist = float.MaxValue;
        m_impactPoints.Clear();
        SteamVR_TrackedObject closerController = null;
        foreach(SteamVR_TrackedObject i in U3D.SteamVR.Controllers.instance.trackedObjects)
        {
            if (i!= null && i.gameObject.activeSelf)
            {
                RaycastHit info;
                Vector3 end = i.transform.TransformVector(Vector3.forward * 100);
                if (Physics.Linecast(end, i.transform.position, out info))
                {
                    m_impactPoints.Add(info.point);
                    float currentDist = Vector3.Distance(info.point, m_cowPosition);
                    if (currentDist < dist)
                    {
                        dist = currentDist;
                        closerController = i;
                    }
                }
            }
        }

        bool hit = false;
        if(dist == float.MaxValue)
        {
            m_audioSource.Stop();
            m_audioSource.clip = null;
        }
        else
        {
            int index;
            if(dist > m_maxDistance)
            {
                dist = 1;
                index = 0;
            }
            else
            {
                dist /= m_maxDistance;
            }

            index = clips.Count - Mathf.RoundToInt(dist * clips.Count);
            if (index >= clips.Count)
            {
                hit = true;
                index = clips.Count - 1;
            }

            if (!m_audioSource.isPlaying)
            {
                m_audioSource.clip = clips[index];
                m_audioSource.Play();
            }

            SteamVR_Controller.Device controller = SteamVR_Controller.Input((int)closerController.index);
            if(controller!= null)
            {
                //float fHaptic = Mathf.Lerp(100, 800, 1 - dist);
                //ushort haptic = (ushort)(fHaptic);
                //controller.TriggerHapticPulse(haptic);

                if (hit)
                {
                    controller.TriggerHapticPulse();
                }

                if (controller.GetPressDown(Valve.VR.EVRButtonId.k_EButton_SteamVR_Trigger))
                {
                    controller.TriggerHapticPulse(500, Valve.VR.EVRButtonId.k_EButton_SteamVR_Trigger);
#if !DEBUG
                    if(hit)
#endif
                    {
                        this.enabled = false;
                        m_audioSource.Stop();
                        foundCow.enabled = true;
                    }
                }
            }
        }
    }
}
