﻿#if DEBUG
//#define LOG_CONSOLE_FoundCow
//#define LOG_GUI_FoundCow
#endif
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(SpriteRenderer))]
[RequireComponent(typeof(AudioSource))]
[RequireComponent(typeof(Animation))]
public class FoundCow : MonoBehaviourBase
{
    #region log stuff
    [System.Diagnostics.Conditional("LOG_CONSOLE_FoundCow")]
    void Log(string format, params object[] args)
    {
        Debug.LogFormat(this.GetType().Name + "\t" + format, args);
    }

    [System.Diagnostics.Conditional("LOG_GUI_FoundCow")]
    void GUILog(string format, params object[] args)
    {
#if LOG_GUI_FoundCow
			m_logStr = string.Format(format, args);
#endif
    }
#if LOG_GUI_FoundCow
		string m_logStr = "";

		public void OnGUI()
		{
			GUIStyle style = new GUIStyle();
			style.normal.textColor = Color.red;
			style.alignment = TextAnchor.LowerLeft;
			GUI.Label(new Rect(5, 5, Screen.width - 10, Screen.height - 10), m_logStr, style);
		}
#endif
    #endregion

    public AudioClip found;
    AudioSource __audioSource;
    AudioSource m_audioSource { get { return __audioSource ?? (__audioSource = GetComponent<AudioSource>()); } }

    SpriteRenderer __sprite;
    SpriteRenderer m_sprite { get { return __sprite ?? (__sprite = GetComponent<SpriteRenderer>()); } }

    Animation __animation;
    Animation m_animation { get { return __animation ?? (__animation = GetComponent<Animation>()); } }

    public List<HideShowCanvas> hideShow;
    public void OnEnable()
    {
        m_audioSource.clip = found;
        m_audioSource.Play();
        m_sprite.enabled = true;
        m_animation.Play("cowFound");

        int v = PlayerPrefs.GetInt("PlayerScores", 0);
        v++;
        PlayerPrefs.SetInt("PlayerScores", v);
        PlayerPrefs.Save();

        U3D.SteamVR.Controllers.instance.anyTrigger += TriggerPressed;

        hideShow.ForEach(a =>
        {
            a.Show();
        });
        onFound.Invoke();
    }

    public void OnDisable()
    {
        m_sprite.enabled = false;
        U3D.SteamVR.Controllers.instance.anyTrigger -= TriggerPressed;

        hideShow.ForEach(a => a.Hide());
    }

    public Cow cow;
    private void TriggerPressed()
    {
        this.enabled = false;
        cow.enabled = true;
    }

    public UnityEvent onFound;
}
