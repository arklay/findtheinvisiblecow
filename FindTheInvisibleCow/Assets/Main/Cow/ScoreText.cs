﻿using System;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class ScoreText : MonoBehaviourBase
{
    Text __text;
    Text m_text { get { return __text ?? (__text = GetComponent<Text>()); } }

    string m_originalText;
    public string stringToReplace = "<<SCORE>>";

    public void OnEnable()
    {
        Refresh();
    }

    public void Refresh()
    {
        if (string.IsNullOrEmpty(m_originalText))
            m_originalText = m_text.text;

        string v = PlayerPrefs.GetInt("PlayerScores", 0).ToString();
        m_text.text = m_originalText.Replace(stringToReplace, v);
    }
}