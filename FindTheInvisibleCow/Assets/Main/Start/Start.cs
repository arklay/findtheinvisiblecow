﻿using System;
using UnityEngine;

[RequireComponent(typeof(Animation))]
public class Start : MonoBehaviourBase
{
    public void OnEnable()
    {
        U3D.SteamVR.Controllers.instance.anyTrigger += TriggerPressed;
    }

    public void OnDisable()
    {
        U3D.SteamVR.Controllers.instance.anyTrigger -= TriggerPressed;
    }

    Animation __animation;
    Animation m_animation { get { return __animation ?? (__animation = GetComponent<Animation>()); } }

    public Cow cow;
    private void TriggerPressed()
    {
        this.enabled = false;
        m_animation.Play("Hide");
    }

    public void HideFinished()
    {
        cow.enabled = true;
    }
}
